<?php

require_once("MrScraper.php");

function parsePage($url) // parse page Item
{
  var_dump($url);
  $page = MrScraper::get($url);
  $data = [];
  $data['url'] = $url;
  $data['location'] = explode('،', $page->query("//*[contains(@class, 'show_area')]")[0]->textContent)[0];
  $data['city'] = explode('،', $page->query("//*[contains(@class, 'show_area')]")[0]->textContent)[1];
  $data['unit_type'] = explode(' ', $page->query("//li /p /span /a")[1]->textContent)[0];
  $data['post_type'] = explode(' ', $page->query("//li /p /span /a")[1]->textContent)[1];
  $data['area'] = $page->query("//*[contains(@class, 'show_size')]")[0]->textContent;
  $data['price'] = $page->query("//*[contains(@class, 'show_price')]")[0]->textContent;
  $data['description'] =
            $page->query("//*[contains(@class, '.hdline')] /*[contains(@class, 'show_title')]")[0]->textContent
            . " "
            . $page->query("//*[contains(@class, 'show_description')]")[0]->textContent;
  MrScraper::processRealestate($data);
}

function parseList($url) // Parse the lists and loop throup items
{
  $list = MrScraper::get($url);
  foreach ($list->query("//div[contains(@class, 'listing-title')] //a /@href") as $link)
  {
    $page_url = $link->nodeValue;
    $page_url = MrScraper::cleanUrl($page_url, $url);
    $last_part = array_pop(explode("/", $page_url));
    $encoded_last_part = urlencode($last_part);
    $page_url = str_replace($last_part, $encoded_last_part, $page_url);
    $new_url = $page_url;
    parsePage($new_url);
  }
}

function loop() // Loop through the lists
{
  $max_page_number_limit = 200;
  $base_url = "http://eg.waseet.net/ar/site/cairo/real_estate/all/listing?page=";
  for($page_number=1; $page_number <= $max_page_number_limit; $page_number++)
  {
    $url = $base_url.$page_number;
    parseList($url);
  }

}

loop();

?>
