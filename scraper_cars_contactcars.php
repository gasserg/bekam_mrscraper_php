<?php

require_once("MrScraper.php");

function parsePage($url) // parse page Item
{
  $page = MrScraper::get($url);
  $data = [];
  $data['url'] = $url;
  $data['make'] = $page->query("//text[@itemprop='name']/text()")[1]->nodeValue;
  $data['model'] = $page->query("//text[@itemprop='name']/text()")[2]->nodeValue;
  $data['year'] = $page->query("//b[contains(text(), 'سنة الصنع')]/parent::*/parent::*/td[2]/text()")[0]->nodeValue;
  $data['price'] = $page->query("//div[contains(@class, 'orange')]//span/text()")[0]->nodeValue;
  $data['description'] = "No Description...";
  MrScraper::processCar($data);
}

function parseList($url) // Parse the lists and loop throup items
{
  $list = MrScraper::get($url);
  foreach ($list->query("//a[contains(@class, 'u-list')] /@href") as $link)
  {
    $new_url = $link->nodeValue;
    $new_url = MrScraper::cleanUrl($new_url, $url);
    parsePage($new_url);
  }
}

function loop() // Loop through the lists
{
  $max_page_number_limit = 390;
  $base_url = 'https://www.contactcars.com/usedcars?page=';
  for($page_number=1; $page_number <= $max_page_number_limit; $page_number++)
  {
    $url = $base_url.$page_number;
    parseList($url);
  }
}

loop();

?>
