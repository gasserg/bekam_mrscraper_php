<?php

require_once("MrScraper.php");

function parsePage($url) // parse page Item
{
  $page = MrScraper::get($url);
  $data = [];
  $data['url'] = $url;
  // $data['location'] =
  //                     $page->query("//*[@id='breadcrumbs'] //a")[1]->textContent
  //                   . " "
  //                   . $page->query("//*[@id='breadcrumbs'] //a")[2]->textContent
  //                   . " "
  //                   . $page->query("//*[@id='breadcrumbs'] //a")[3]->textContent;
  // $data['city'] = $page->query("//*[@id='breadcrumbs'] //a")[0]->textContent;

  $data['city'] = $page->query("//nav[@id='breadcrumbs']//span")[0]->textContent;
  $data['location'] = $page->query("//nav[@id='breadcrumbs']//span")[1]->textContent;

  $data['area'] = explode('/', $page->query("//th[contains(text(), 'المساحة')] /parent::* /td")[0]->textContent)[0];
  $data['unit_type'] = $page->query("//th[contains(text(), 'نوع العقار')] /parent::* /td")[0]->textContent;
  $data['price'] = $page->query("//span[contains(@class, 'price')]")[0]->textContent;

  $data['post_type'] = explode('/', explode('//', urldecode($data['url']))[1])[2] ;

  $data['description'] = $page->query("//*[contains(@class, 'primary')] //h1")[0]->textContent
                  . " "
                  . $page->query("//*[contains(@class, 'property-description-area')] /section")[0]->textContent;
  MrScraper::processRealestate($data);
}

function parseList($url) // Parse the lists and loop throup items
{
  $list = MrScraper::get($url);
  foreach ($list->query("//div[contains(@class, 'property-content')] /h2 /a[not(contains(@href, '{{url}}'))] /@href") as $link)
  {
    $page_url = $link->nodeValue;
    $page_url = MrScraper::cleanUrl($page_url, $url);
    $new_url = $page_url;
    parsePage($new_url);
  }
}

function loop() // Loop through the lists
{
  $max_page_number_limit = 200;
  $base_url = "https://www.propertyfinder.eg/ar/search?page=";
  for($page_number=1; $page_number <= $max_page_number_limit; $page_number++)
  {
    $url = $base_url.$page_number;
    parseList($url);
  }

}

loop();

?>
