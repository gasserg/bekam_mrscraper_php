# Bekam? - Mr. Scraper (written in php)

## Description
This is the scraper for Bekam ? http://bekam-eg.com, rewritten in PHP

## To Do List
* [x] Add counters to the website
* [X] Add Failsafe to get() repeater, a maximum retries.
* [X] Add a method to skip  the already-scraped URLs
* [ ] Add more validation:
  * [ ] Cars
  * [ ] Realestate
* [ ] Add a method to log missing corrections:
  * [ ] Cars:
    * [ ] Car makes
    * [ ] Car Models
  * [ ] Realestate:
    * [ ] Realestate Unit Types
    * [ ] Realestate Post Types
