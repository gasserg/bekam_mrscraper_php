<?php

require_once("MrScraper.php");

function parsePage($url) // parse page Item
{
  $page = MrScraper::get($url);
  $data = [];
  $data['url'] = $url;
  $data['make'] = $page->query("//li /a /span")[4]->textContent;
  foreach (explode(" ", $page->query("//li /a /span")[3]->textContent) as $part)
  {
    $data['make'] = str_replace($part, '', $data['make']);
  }
  $data['model'] = $page->query("//th[contains(text(), 'موديل')] /parent::* /td")[0]->textContent;
  $data['year'] = $page->query("//th[contains(text(), 'السنة')] /parent::* /td")[0]->textContent;

  $data['description'] = $page->query("//h1")[0]->textContent . $page->query("//div[contains(@id, 'textContent')]")[0]->textContent;
  $data['price'] = $page->query("//div[contains(@class, 'pricelabel')]")[0]->textContent;
  MrScraper::processCar($data);
}

function parseList($url) // Parse the lists and loop throup items
{
  $list = MrScraper::get($url);
  foreach ($list->query("//a[contains(@class, 'ads__item__title')]/@href") as $link)
  {
    $new_url = $link->nodeValue;
    parsePage($new_url);
  }
}

function loop() // Loop through the lists
{
  $max_page_number_limit = 500;
  $base_url = 'https://olx.com.eg/vehicles/cars/?page=';
  for($page_number=1; $page_number <= $max_page_number_limit; $page_number++)
  {
    $url = $base_url.$page_number;
    parseList($url);
  }
}

loop();

?>
