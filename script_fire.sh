echo "[$(date)] This script starts all the scrapers in series (one after one)"


cd $(dirname $0)
mkdir logs;

for i in scraper_* ; do
  echo "[$(date)] Starting $i"
  php --php-ini php.ini $i > "logs/$i.log";

  echo "[$(date)] Starting scraper_cleaner"
  php --php-ini php.ini scraper_cleaner.php > "logs/scraper_cleaner.log"

  echo "[$(date)] sleeping for 900 seconds (15mins)"
  sleep 900 ;
done;
