<?php

require_once("MrScraper.php");

function parseList($url) // Parse the lists and loop throup items
{
  $list = MrScraper::get($url);
  foreach ($list->query("//div[contains(@class, 'panel-body')] //div[contains(@class, 'col-sm-12')] /div[contains(@class, 'well')]") as $post)
  {
    $data = [];
    $data['url'] = $list->query(".//a/@href", $post)[0]->nodeValue;
    $data['make'] = $list->query(".//li[@itemprop = 'manufacturer']", $post)[0]->textContent;
    $data['model'] = $list->query(".//li[@itemprop = 'model']", $post)[0]->textContent;
    $data['year'] = $list->query(".//span[contains(text(), 'سنة')]", $post)[0]->textContent;
    $data['price'] = $list->query(".//span[contains(@class, 'price')]", $post)[0]->textContent;
    $data['description'] = $list->query(".//a /h4", $post)[0]->textContent;
    MrScraper::processCar($data);
  }
}

function loop() // Loop through the lists
{
  $max_page_number_limit = 200;
  $base_url = 'http://eg.waseet.net/ar/site/all/cars/all/search/?price_from=5000&price_to=99999999&page=';
  for($page_number=1; $page_number <= $max_page_number_limit; $page_number++)
  {
    $url = $base_url.$page_number;
    parseList($url);
  }
}

loop();

?>
