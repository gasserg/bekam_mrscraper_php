#!/bin/sh

# This script starts script_fire if it's not working

cd $(dirname $0)

if ( ps -ef | grep -v grep | grep script_fire.sh ) then
  exit 0
else
  mkdir logs;
  sh script_fire.sh > "logs/log.log";
fi
