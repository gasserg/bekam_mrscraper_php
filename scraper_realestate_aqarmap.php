<?php

require_once("MrScraper.php");

function parsePage($url) // parse page Item
{
  $page = MrScraper::get($url);
  $data = [];
  $data['url'] = $url;
  // $data['location_array'] = [];
  // foreach ($page->query("//*[contains(@class, 'breadcrumb')] //li //a") as $i) {
  //   if (strpos($i->textContent, ' ب') !== false)
  //   {
  //     foreach (explode( ' ب' , $i->textContent) as $ii)
  //     {
  //       $data['location_array'][] = $ii;
  //     }
  //   } else {
  //     $data['location_array'][] = $i->textContent;
  //   }
  // }
  //
  // $data['city'] = $data['location_array'][4];
  // $data['location'] = $data['location_array'][6];

  $city_array = explode( ' ب' , $page->query("//*[contains(@class, 'breadcrumb')] //li //a")[3]->textContent);
  $data['city'] = $city_array[count($city_array) - 1];
  $location_array = explode( ' ب' , $page->query("//*[contains(@class, 'breadcrumb')] //li //a")[4]->textContent);
  $data['location'] = $location_array[count($location_array) - 1];;

  if($data['city'] == null or $data['location'] == null or $data['city'] == '' or $data['location'] == '')
  {
    $city_and_location = $page->query("//div[contains(@class, 'titleAndAddress')] //p")[0]->textContent;
    var_dump($city_and_location);
    var_dump( explode('،', $city_and_location) );
    $data['location'] = explode('،', $city_and_location)[0];
    $data['city'] = explode('،', $city_and_location)[1];
  }

  global $cities_corrections;
  if( MrScraper::suggestCorrection($data['city'], $cities_corrections) == false )
  {
    $data['location'] = $data['city'];
    $data['city'] = "القاهرة";
  }

  $data['area'] = $page->query("//td[contains(text(), 'المساحة')] /parent::* /td")[1]->textContent;
  $data['price'] = $page->query("//td[contains(text(), 'السعر')] /parent::* /td")[1]->textContent;
  $data['unit_type'] = $page->query("//td[contains(text(), 'النوع')] /parent::* /td")[1]->textContent;

  $data['post_type'] = explode( "لل" , $page->query("//*[contains(@class, 'breadcrumb')] //li //a")[2]->textContent)[1];
  $data['description'] = $page->query("//*[contains(@class, 'titleAndAddress')] //h1")[0]->textContent
                        . $page->query("//*[contains(@class, 'listingDesc')]")[0]->textContent;
  MrScraper::processRealestate($data);
}

function parseList($url) // Parse the lists and loop throup items
{
  $list = MrScraper::get($url);
  foreach ($list->query("//table //tr //a /@href") as $link)
  {
    $page_url = $link->nodeValue;
    $page_url = MrScraper::cleanUrl($page_url, $url);
    $new_url = $page_url;
    parsePage($new_url);
  }
}

function loop() // Loop through the lists
{
  $max_page_number_limit = 1000;
  $base_url = "https://egypt.aqarmap.com/ar/listing/latest?page=";
  for($page_number=1; $page_number <= $max_page_number_limit; $page_number++)
  {
    $url = $base_url.$page_number;
    parseList($url);
  }
}

loop();

?>
