<?php

require_once("MrScraper.php");

function parseList($url) // Parse the lists and loop throup items
{
  $list = MrScraper::get($url);
  foreach ($list->query("//div[contains(@class, 'CarListWrapper')]/div[contains(@class, 'CarListUnit')]") as $post)
  {
    $data = [];
    $data['url'] = $list->query(".//a[contains(@class, 'NewListTitle')]/@href", $post)[0]->textContent;
    $data['url'] = MrScraper::cleanUrl($data['url'], $url);

    $data['make'] = $list->query(".//div[contains(@class, 'pdListDataMetas')] /span", $post)[0]->textContent;
    $data['model'] = $list->query(".//div[contains(@class, 'pdListDataMetas')] /span", $post)[1]->textContent;
    $data['year'] = $list->query(".//div[contains(@class, 'pdListDataMetas')] /span", $post)[2]->textContent;

    $data['make'] .= $list->query(".//ul[contains(@class, 'NewListSpecifications')] /li", $post)[0]->textContent;
    $data['model'] .= $list->query(".//ul[contains(@class, 'NewListSpecifications')] /li", $post)[1]->textContent;
    $data['year'] .= $list->query(".//ul[contains(@class, 'NewListSpecifications')] /li", $post)[2]->textContent;

    $data['price'] = $list->query(".//*[contains(@class, 'pdListPrice')]", $post)[0]->textContent;
    $data['price'] .= $list->query(".//*[contains(@class, 'NewListPrice')]", $post)[0]->textContent;

    $data['description'] = $list->query(".//a[contains(@class, 'NewListTitle')]", $post)[0]->textContent;
    MrScraper::processCar($data);
  }
}

function loop() // Loop through the lists
{
  $max_page_number_limit = 500;
  $base_url = 'https://eg.hatla2ee.com/ar/car/page/';
  for($page_number=1; $page_number <= $max_page_number_limit; $page_number++)
  {
    $url = $base_url.$page_number;
    parseList($url);
  }
}

loop();

?>
