<?php

require_once("MrScraper.php");

function parsePage($url, $args = []) // parse page Item
{
  $page = MrScraper::get($url);
  $data = [];
  $data['url'] = $url;

  $city_and_location = $page->query("//div[contains(@class, 'offerheadinner')] //p //span /strong")[0]->textContent;

  $data['location'] = explode('،', $city_and_location)[0];
  $data['city'] = explode('،', $city_and_location)[1];

  $data['area'] = $page->query("//th[contains(text(), 'المساحة')] /parent::* /td")[0]->textContent;

  if($args['type'] == "Sale")
  {
    $data['unit_type'] = explode( "للبيع", $page->query("//table[contains(@class, 'breadcrumb')] //ul //li")[3]->textContent)[0];
    $data['post_type'] = "Sale";
  }
  elseif($args['type'] == "Rent")
  {
    $data['unit_type'] = explode( "للإيجار", $page->query("//table[contains(@class, 'breadcrumb')] //ul //li")[3]->textContent)[0];
    $data['post_type'] = "Rent";
  }

  $data['price'] = $page->query("//div[contains(@class, 'pricelabel')]")[0]->textContent;
  $data['description'] =
    $page->query("//div[contains(@class, 'offerheadinner')] //h1")[0]->textContent
    . $page->query("//div[contains(@class, 'offerdescription')] //div[contains(@class, 'descriptioncontent')] //p")[0]->textContent;
  MrScraper::processRealestate($data);
}

function parseList($url, $args = []) // Parse the lists and loop throup items
{
  $list = MrScraper::get($url);
  foreach ($list->query("//a[contains(@class, 'ads__item__title')] /@href") as $link)
  {
    $new_url = $link->nodeValue;
    parsePage($new_url, $args);
  }
}

function loop() // Loop through the lists
{
  $max_page_number_limit = 500;
  $base_url = "https://olx.com.eg/properties/properties-for-rent/?page=";
  for($page_number=1; $page_number <= $max_page_number_limit; $page_number++)
  {
    $url = $base_url.$page_number;
    parseList($url, ['type' => "Rent", ]);
  }
  $base_url = "https://olx.com.eg/properties/properties-for-sale/?page=";
  for($page_number=1; $page_number <= $max_page_number_limit; $page_number++)
  {
    $url = $base_url.$page_number;
    parseList($url, ['type' => "Sale", ]);
  }
}

loop();

?>
