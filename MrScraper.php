<?php

/*
This is the main scraping Class.
It has several static methods to
 get DOMXPath, clean, connect to
 DB and Store.
*/

require("car_make_corrections.php");
require("car_model_corrections.php");
require("realestate_post_type_corrections.php");
require("realestate_unit_type_corrections.php");
require("settings.php");

$cities_corrections = require("./cities_corrections.php");

class MrScraper
{
    static public $_conn = null;

    protected function __construct() {}

    protected function __clone() {}

    static public function log($message)
    {
      print("[".date("Y-m-d H:i:s")."] ".$message."\n");
      return true;
    }

    static public function get($url)
    {
      global $sleep_time;
      self::log("Waiting $sleep_time seconds before sending request");
      sleep($sleep_time);

      $opts = array(
              'http' => array(
                  'method'  => 'GET',
                  'user_agent'  => "Mozilla/5.0 (Windows NT x.y; Win64; x64; rv:10.0) Gecko/20100101 Firefox/10.0",
                  'header' => array('Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*\/*;q=0.8'),
                  'timeout' => 15,
                  'accept-language' => 'ar',
              )
          );
      $context  = stream_context_create($opts);

      $html = false;
      $counter = 1;
      while($html == false or $html == null or $html == "")
      {
        if($counter > 5)
        {
          self::log("[ERROR] [Trial $counter] FAILED AFTER $counter TRIALS. EXITING!! " . $url);
          exit();
        }
        self::log(" [Trial $counter] Trying to get " . $url);
        $html = file_get_contents($url, false, $context);
        self::log("Done getting " . $url);
        if($html == false or $html == null or $html == "")
        {
          self::log("Failed to get page. Will repeat after 10 seconds : ".$url);
          $counter += 1;
          sleep(10);
        }
      }
      $html = mb_convert_encoding($html, 'HTML-ENTITIES', "UTF-8");
      $dom_doc = new DOMDocument();
      @$dom_doc->loadHTML($html);
      $doc_xpath = new DOMXPath($dom_doc);
      $repeat = false;
      self::log("Got page ".$url);
      return $doc_xpath;
    }

    // Cleans a url. Usefull for partial hrefs.
    // Input string $partial_url
    // Input string $current_url
    // output string
    function cleanUrl($partial_url, $current_url)
    {
        $partial_url = str_replace("\n", '', $partial_url);
        $partial_url = trim($partial_url, " \t\n\r\0\x0B");
        if (substr($partial_url, 0, 4) !== "http")
        {
            if (substr($partial_url, 0, 1) == "/")
            {
                $base_url = explode("//", $current_url)[0]."//".explode("/", explode("//", $current_url)[1])[0];
                $out = $base_url . $partial_url;
            } else {
              $out = $current_url . $partial_url;
            }
        }
        else
        {
            $out = $partial_url;
        }
        $out = str_replace("///", "/", $out);
        $out = str_replace("//", "/", $out);
        $out = str_replace(":/", "://", $out);
        $out = str_replace(' ', '%20', $out);
        return $out;
    }

    static public function conn()
    {
      if(self::$_conn == null)
      {
        global $db_host;
        global $db_user;
        global $db_password;
        global $db_name;

        self::$_conn = new PDO("mysql:host=$db_host;dbname=$db_name;charset=utf8", $db_user, $db_password);
        self::$_conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        self::$_conn->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        self::$_conn->setAttribute(PDO::ATTR_AUTOCOMMIT, True);
      }
      return self::$_conn;
    }

    // Checks if the url is in the name table
    // Input: string $url
    // Input: string $table_name
    // Output: Boolean
    static public function checkUrlIsScraped($url, $table_name)
    {
      $count = self::query("SELECT count(*) as 'count' FROM $table_name WHERE url = ?;", [$url, ])[0]['count'];
      if($count == "0" or $count == 0)
      {
        return false;
      } else {
        return true;
      }
    }

    static public function query($query, $params = [])
    {
      $r = self::conn()->prepare($query);
      $r->execute($params);
      return $r->fetchAll();
    }

    static public function execute($query, $params = [])
    {
      $r = self::conn()->prepare($query);
      $r->execute($params);
      return True;
    }

    // Finds the most suitable correction from a list
    // input: string $input
    // input: array(pew => pewpew,...) $list
    // output: string or False
    public static function suggestCorrection($input, $list)
    {
      $best_percentage = 0;
      $best_old = "";
      $best_new = "";

      foreach($list as $old => $new)
      {
        $percentage = 0;
        similar_text(trim(strtolower($input)) , trim(strtolower($old)) , $percentage);
        if($percentage > $best_percentage)
        {
          $best_percentage = $percentage;
          $best_old = $old;
          $best_new = $new;
        }
      }
      if($best_percentage > 90.0)
      {
        self::log("The best match for '$input' is '$best_old' => '$best_new' with similarity $best_percentage %");
        return $best_new;
      } else {
        self::log("[!] Unacceptable match! The best match for '$input' is '$best_old' => '$best_new' with similarity $best_percentage %");
        return false;
      }
    }

    static public function cleanInteger($input)
    {
      $input = trim($input);
      $input = str_replace("\n", '', $input);
      $input = str_replace("  ", ' ', $input);
      $input = str_replace(',', '', $input);
      $input = str_replace('٠', '0', $input);
      $input = str_replace('١', '1', $input);
      $input = str_replace('٢', '2', $input);
      $input = str_replace('٣', '3', $input);
      $input = str_replace('٤', '4', $input);
      $input = str_replace('٥', '5', $input);
      $input = str_replace('٦', '6', $input);
      $input = str_replace('٧', '7', $input);
      $input = str_replace('٨', '8', $input);
      $input = str_replace('٩', '9', $input);
      $input = preg_replace("/[^0-9]+/", '', $input);
      return $input;
    }

    static public function cleanString($input)
    {
      $input = str_replace("أ", "ا", $input);
      $input = str_replace("آ", "ا", $input);
      $input = str_replace("إ", "ا", $input);

      $input = str_replace("ي ", "ى ", $input);

      while(strpos($input, "\n") !== false)
      {
        $input = str_replace("\n", ' ', $input);
      }
      while(strpos($input, "\t") !== false)
      {
        $input = str_replace("\t", ' ', $input);
      }
      while(strpos($input, "  ") !== false)
      {
        $input = str_replace("  ", ' ', $input);
      }
      $input = trim($input);
      return $input;
    }

    static public function processCar($data)
    {
      global $car_make_corrections;
      global $car_model_corrections;

      $everythin_alright_till_now = True;
      $message = "";
      $data['url'] = $data['url'];
      $data['url_hash'] = hash('sha512', $data['url']);
      $data['year'] = self::cleanInteger($data['year']);
      $data['make'] = self::cleanString($data['make']);
      $data['make'] = str_replace('.', '', $data['make']);
      $data['model'] = self::cleanString($data['model']);
      $data['model'] = str_replace('.', '', $data['model']);
      $data['price'] = self::cleanInteger($data['price']);
      $data['description'] = self::cleanString($data['description']);


      $new_make = self::suggestCorrection($data['make'], $car_make_corrections);
      $new_model = self::suggestCorrection($data['model'], $car_model_corrections);

      if($new_make !== false)
      {
        $data["make"] = $new_make;
      } else {
        $message .= "Didn't find make ".$data['make'].". ";
        $everythin_alright_till_now = False;
      }
      if($new_model !== false)
      {
        $data["model"] = $new_model;
      } else {
        $message .= "Didn't find model ".$data['model'].". ";
        $everythin_alright_till_now = False;
      }

      // Validate
      $error_message = "";
      foreach (['make', 'model', 'year', 'url', 'price', 'description'] as $i)
      {
        if($data[$i] == null or $data[$i] == "")
        {
          $error_message .= "$i not found! ";
        }
      }
      if($error_message != "")
      {
        $message .= "[!] ".$error_message;
        $everythin_alright_till_now = False;
      }

      // store
      if($everythin_alright_till_now)
      {
        $message .= "Everything is OK. ";
        if(self::checkUrlIsScraped($data['url'], 'cars'))
        {
          $sql = "UPDATE IGNORE cars set
                  make = ? ,
                  model = ? ,
                  year = ? ,
                  description = ? ,
                  price = ? ,
                  updated_at = NOW()
                  WHERE url = ?;";
          $params = [
            $data['make'],
            $data['model'],
            $data['year'],
            $data['description'],
            $data['price'],
            $data['url'],
          ];
          self::execute($sql, $params);
          $message .= "Car updated. ";
        } else {
          $sql = "INSERT IGNORE INTO cars (
                  url, url_hash, make, model, year, description, price, created_at, updated_at)
                  values (?, ?, ?, ?, ?, ?, ?, NOW(), NOW());";
          $params = [
            $data['url'],
            $data['url_hash'],
            $data['make'],
            $data['model'],
            $data['year'],
            $data['description'],
            $data['price'],
          ];
          self::execute($sql, $params);
          $message .= "Car Created. ";
        }
      } else {
        $message = "[ERROR] " . $message;
      }
      self::log("Processed Car \"".$data['url']."\". ".$message."\n");
      var_dump($data);
    }

    static public function processRealestate($data)
    {
      global $realestate_post_type_corrections;
      global $realestate_unit_type_corrections;
      global $cities_corrections;

      // do some replacements
      $data['city'] = str_replace("محافظة", "", $data['city']);
      $data['location'] = str_replace("محافظة", "", $data['location']);

      $data['city'] = str_replace("الكبرى", "", $data['city']);

      $data['city'] = explode("-", $data['city'])[0];
      $data['location'] = explode("-", $data['location'])[0];

      // corrections
      $everythin_alright_till_now = True;
      $message = "";
      $data['url'] = $data['url'];
      $data['url_hash'] = hash('sha512', $data['url']);
      $data['area'] = self::cleanInteger($data['area']);
      $data['city'] = self::cleanString($data['city']);
      $data['location'] = self::cleanString($data['location']);
      $data['price'] = self::cleanInteger($data['price']);
      $data['description'] = self::cleanString($data['description']);
      $data['post_type'] = self::cleanString($data['post_type']);
      $data['unit_type'] = self::cleanString($data['unit_type']);

      $new_post_type = self::suggestCorrection($data['post_type'], $realestate_post_type_corrections);
      if($new_post_type !== false)
      {
        $data["post_type"] = $new_post_type;
      } else {
        $message .= "Didn't find post_type ".$data['post_type'].". ";
        $everythin_alright_till_now = False;
      }

      $new_unit_type = self::suggestCorrection($data['unit_type'], $realestate_unit_type_corrections);
      if($new_unit_type !== false)
      {
        $data["unit_type"] = $new_unit_type;
      } else {
        $message .= "Didn't find unit_type ".$data['unit_type'].". ";
        $everythin_alright_till_now = False;
      }

      // cities and locations are tricky
      $new_city = self::suggestCorrection($data['city'], $cities_corrections);
      if($data['city'] == "" or $new_city == false)
      {
        $new_city = self::suggestCorrection($data['location'], $cities_corrections);
      }
      if($new_city !== false)
      {
        $data["city"] = $new_city;
      } else {
        $message .= "Didn't find city ".$data['city'].". ";
        $everythin_alright_till_now = False;
      }

      // Validate
      $error_message = "";
      foreach (['unit_type', 'post_type', 'area', 'url', 'price', 'description', 'location', 'city'] as $i)
      {
        if($data[$i] == null or $data[$i] == "")
        {
          $error_message .= "$i not found! ";
        }
      }
      if($error_message != "")
      {
        $message .= "[!] ".$error_message;
        $everythin_alright_till_now = False;
      }

      // Store
      if($everythin_alright_till_now)
      {
        $message .= "Everything is OK. ";
        if( self::checkUrlIsScraped($data['url'], 'realestates') )
        {
          $sql = "UPDATE IGNORE realestates SET
                  city = ? ,
                  location = ? ,
                  area = ? ,
                  unit_type = ? ,
                  post_type = ? ,
                  description = ? ,
                  price = ? ,
                  updated_at = NOW()
                  where url = ? ;";
          $params = [
            $data['city'],
            $data['location'],
            $data['area'],
            $data['unit_type'],
            $data['post_type'],
            $data['description'],
            $data['price'],
            $data['url'],
          ];
          self::execute($sql, $params);
          $message .= "Post Updated. ";
        } else {
          $sql = "INSERT IGNORE INTO realestates (
                  url, url_hash, city, location, area, unit_type, post_type, description, price, created_at, updated_at)
                  values (?, ?, ?, ?, ?, ?, ?, ?, ?, NOW(), NOW());";
          $params = [
            $data['url'],
            $data['url_hash'],
            $data['city'],
            $data['location'],
            $data['area'],
            $data['unit_type'],
            $data['post_type'],
            $data['description'],
            $data['price'],
          ];
          self::execute($sql, $params);
          $message .= "Post Created. ";
        }
      } else {
        $message = "[ERROR] " . $message;
      }
      self::log("Processed realestate \"".$data['url']."\". ".$message."\n");
      var_dump($data);
    }
}

?>
