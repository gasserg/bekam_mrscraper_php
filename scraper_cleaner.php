<?php

require_once("MrScraper.php");

function clean() // parse page Item
{
  MrScraper::log("Removing old CARS");
  MrScraper::execute("DELETE FROM cars WHERE user_id IS NULL AND updated_at < NOW() - INTERVAL 3 DAY;");
  MrScraper::execute("DELETE FROM cars WHERE user_id IS NULL AND updated_at IS NULL AND created_at < NOW() - INTERVAL 3 DAY;");
  MrScraper::log("Removing old REAL ESTATE");
  MrScraper::execute("DELETE FROM realestates WHERE user_id IS NULL AND updated_at < NOW() - INTERVAL 3 DAY;");
  MrScraper::execute("DELETE FROM realestates WHERE user_id IS NULL AND updated_at IS NULL AND created_at < NOW() - INTERVAL 3 DAY;");
}

clean();

?>
